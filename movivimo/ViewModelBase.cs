﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Movivimo
{
    /// <summary>
    /// A base class for view models.
    /// </summary>
    public abstract class ViewModelBase: IViewModel
    {
        private bool _disposed;

        public event PropertyChangedEventHandler PropertyChanged;

        public void Dispose()
        {
            if (!_disposed)
            {
                OnDispose();
                _disposed = true;
            }
        }

        protected void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        protected virtual void OnDispose()
        {
        }
    }
}

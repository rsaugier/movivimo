﻿using System;
using System.ComponentModel;

namespace Movivimo
{
    /// <summary>
    /// An interface for view models.
    /// </summary>
    public interface IViewModel: INotifyPropertyChanged, IDisposable
    {
        
    }
}
